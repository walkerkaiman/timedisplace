import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.video.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class TimeDisplace extends PApplet {



Capture video;
ArrayList<PImage> frames = new ArrayList<PImage>();
int barHeight;

public void setup() {
  
  frameRate(24);

  video = new Capture(this, width, height);
  video.start();  
  
  barHeight = 8;
}

public void draw() {
 int currentImage = 0;
 
 loadPixels();
  
  for(int y = 0; y < video.height; y += barHeight){
    if(currentImage < frames.size()){
      PImage img = frames.get(currentImage);
      
      if(img != null){
        img.loadPixels();

        for(int x = 0; x < video.width; x++){
          for(int i = 0; i < barHeight; i++){
            pixels[x + (y+i) * width] = img.pixels[x + (y+i) * video.width];
          }
        }  
      }
      currentImage++;
    } 
  }
  updatePixels();
}

public void captureEvent(Capture camera) {
  PImage img = createImage(width, height, RGB);
  
  camera.read();
  video.loadPixels();
  arrayCopy(video.pixels, img.pixels);
  
  frames.add(img);
  
  if(frames.size() > height/barHeight){
    frames.remove(0);
  }
}
  public void settings() {  size(640, 480); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "TimeDisplace" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
